package com.andy.produce;


import com.andy.produce.domain.Book;
import com.andy.produce.service.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
class ProduceApplicationTests {

    @Autowired
    private BookService bookService;

    @Test
    public void contextLoads() {
        Book byId = bookService.getById(1L);
        System.out.println(byId);
    }

    @Test
    public void def(){

    }

}
