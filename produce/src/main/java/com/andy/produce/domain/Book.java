package com.andy.produce.domain;

import lombok.Data;

import java.io.Serializable;


@Data
public class Book implements Serializable {

    private static final long serialVersionUID = -1L;

    private Long id;
    private String name;
}
