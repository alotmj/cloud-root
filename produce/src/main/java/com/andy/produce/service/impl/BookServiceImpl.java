package com.andy.produce.service.impl;

import com.andy.produce.domain.Book;
import com.andy.produce.mapper.BookMapper;
import com.andy.produce.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;

public class BookServiceImpl implements BookService {

    @Autowired
    private BookMapper bookMapper;

    @Override
    public Book getById(Long id) {
        return bookMapper.getById(id);
    }
}
