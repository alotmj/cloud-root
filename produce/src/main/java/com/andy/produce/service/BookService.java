package com.andy.produce.service;

import com.andy.produce.domain.Book;
import org.springframework.stereotype.Service;

@Service
public interface BookService {
    Book getById(Long id);
}
