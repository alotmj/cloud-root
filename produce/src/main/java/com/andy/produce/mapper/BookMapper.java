package com.andy.produce.mapper;

import com.andy.produce.domain.Book;


public interface BookMapper {
    Book getById(Long id);
}
